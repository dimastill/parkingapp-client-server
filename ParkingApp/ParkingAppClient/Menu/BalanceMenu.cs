﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
    class BalanceMenu : IMenu
    {
        private static BalanceMenu instance;
        private static HttpClient client;

        BalanceMenu()
        { }
        public static BalanceMenu GetInstance()
        {
            if (instance == null)
                instance = new BalanceMenu();

            Console.Clear();

            return instance;
        }
        public void Display()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"Balance: {Server.GetBalance()}");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1. Back");
        }

        public int GetInput()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Select menu item: ");
            int.TryParse(Console.ReadLine(), out int selectedMenuItem);
            Console.ForegroundColor = ConsoleColor.Gray;

            return selectedMenuItem;
        }

        public IMenu SelectMenuItem(int indexMenuItem)
        {
            try
            {
                switch (indexMenuItem)
                {
                    case 1:
                        return ParkingMenu.GetInstance();
                    default:
                        throw new Exception("Incorrect input");
                }
            }
            catch (Exception ex)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\nError: {ex.Message}. Try again.\n");
                Console.ForegroundColor = ConsoleColor.Gray;
                
                Display();
                
                indexMenuItem = GetInput();
                return SelectMenuItem(indexMenuItem);
            }
        }
    }
}
