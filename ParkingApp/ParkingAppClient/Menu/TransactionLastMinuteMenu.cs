﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
    class TransactionLastMinuteMenu : IMenu
    {
        private static TransactionLastMinuteMenu instance;

        TransactionLastMinuteMenu()
        { }
        public static TransactionLastMinuteMenu GetInstance()
        {
            if (instance == null)
                instance = new TransactionLastMinuteMenu();

            Console.Clear();

            return instance;
        }
        public void Display()
        {
            List<Transaction> transactions = Server.GetTransactionsLastMinute().ToList();
            Console.ForegroundColor = ConsoleColor.DarkGray;
            foreach (Transaction transaction in transactions)
                Console.WriteLine($"Transaction #{transaction.TransactionID} ({transaction.Time})\n" +
                    $"\tVehicle ID: {transaction.IDVehicle}. +{transaction.Sum}$");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine("1. Refresh");
            Console.WriteLine("2. Back");
        }

        public int GetInput()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Select menu item: ");
            int.TryParse(Console.ReadLine(), out int selectedMenuItem);
            Console.ForegroundColor = ConsoleColor.Gray;

            return selectedMenuItem;
        }

        public IMenu SelectMenuItem(int indexMenuItem)
        {
            try
            {
                switch (indexMenuItem)
                {
                    case 1:
                        return GetInstance(); 
                    case 2:
                        return ParkingMenu.GetInstance();
                    default:
                        throw new Exception("Incorrect input");
                }
            }
            catch (Exception ex)
            {
                Console.Clear();
                
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\nError: {ex.Message}. Try again.\n");
                Console.ForegroundColor = ConsoleColor.Gray;
                Display();
                indexMenuItem = GetInput();
                return SelectMenuItem(indexMenuItem);
            }
        }
    }
}
