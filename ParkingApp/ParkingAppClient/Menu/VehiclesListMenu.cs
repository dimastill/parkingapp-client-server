﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
    class VehiclesListMenu : IMenu
    {
        private static VehiclesListMenu instance;

        VehiclesListMenu()
        { }
        public static VehiclesListMenu GetInstance()
        {
            if (instance == null)
                instance = new VehiclesListMenu();

            Console.Clear();

            return instance;
        }
        public void Display()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            foreach (Vehicle vehicle in Server.GetVehicles())
                Console.WriteLine($"{vehicle.GetNameVehicle()}");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("\n1. Back");
        }

        public int GetInput()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Select menu item: ");
            int.TryParse(Console.ReadLine(), out int selectedMenuItem);
            Console.ForegroundColor = ConsoleColor.Gray;

            return selectedMenuItem;
        }

        public IMenu SelectMenuItem(int indexMenuItem)
        {
            try
            {
                switch (indexMenuItem)
                {
                    case 1:
                        return ParkingMenu.GetInstance();
                    default:
                        throw new Exception("Incorrect input");
                }
            }
            catch (Exception ex)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\nError: {ex.Message}. Try again.\n");
                Console.ForegroundColor = ConsoleColor.Gray;

                Display();

                indexMenuItem = GetInput();
                return SelectMenuItem(indexMenuItem);
            }
        }
    }
}
