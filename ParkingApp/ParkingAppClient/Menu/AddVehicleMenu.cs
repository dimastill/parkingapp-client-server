﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
    class AddVehicleMenu : IMenu
    {
        private bool isAdded;
        private static AddVehicleMenu instance;

        AddVehicleMenu()
        { }
        public static AddVehicleMenu GetInstance()
        {
            if (instance == null)
                instance = new AddVehicleMenu();

            Console.Clear();

            return instance;
        }

        public void Display()
        {
            Console.WriteLine("1. Add Car\n" +
                              "2. Add Truck\n" +
                              "3. Add Bus\n" +
                              "4. Add Motorcycle\n" +
                              "5. Back");

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"\nAdd Status: " + (isAdded ? "Added" : "Not added"));
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        public int GetInput()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Select menu item: ");
            int.TryParse(Console.ReadLine(), out int selectedMenuItem);
            Console.ForegroundColor = ConsoleColor.Gray;

            return selectedMenuItem;
        }

        public IMenu SelectMenuItem(int indexMenuItem)
        {
            try
            {
                switch (indexMenuItem)
                {
                    case 1:
                        isAdded = Server.AddVehicle(new Car());
                        return GetInstance();
                    case 2:
                        isAdded = Server.AddVehicle(new Truck());
                        return GetInstance();
                    case 3:
                        isAdded = Server.AddVehicle(new Bus());
                        return GetInstance();
                    case 4:
                        isAdded = Server.AddVehicle(new Motorcycle());
                        return GetInstance();
                    case 5:
                        return ParkingMenu.GetInstance();
                    default:
                        throw new Exception("Incorrect input");
                }
            }
            catch (Exception ex)
            {
                Console.Clear();

                isAdded = false;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\nError: {ex.Message}. Try again.\n");
                Console.ForegroundColor = ConsoleColor.Gray;

                Display();
                indexMenuItem = GetInput();
                return SelectMenuItem(indexMenuItem);

            }
        }
    }
}
