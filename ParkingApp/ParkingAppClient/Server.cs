﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
    static class Server
    {
        private static string serverAppPath = "https://localhost:44370";

        private static HttpClient client = new HttpClient();
        
        public static Settings GetSettings()
        {
            HttpResponseMessage response = client.GetAsync($"{serverAppPath}/api/settings").Result;
            string jsonSettings = response.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<Settings>(jsonSettings);
        }

        static public void PostSettings(Settings settings)
        {
            string jsonNewSettings = JsonConvert.SerializeObject(settings);

            var content = new StringContent(jsonNewSettings, Encoding.UTF8, "application/json");

            client.PostAsync($"{serverAppPath}/api/settings", content).Wait();
        }

        public static double GetBalance()
        {
            HttpResponseMessage response = client.GetAsync($"{serverAppPath}/api/balance").Result;
            string jsonBalance = response.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<double>(jsonBalance);
        }

        public static double GetMoneyEarnedLastMinute()
        {
            HttpResponseMessage response = client.GetAsync($"{serverAppPath}/api/balance/lastminute").Result;
            string jsonMoneyLastMinute = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<double>(jsonMoneyLastMinute);
        }

        public static IEnumerable<Transaction> GetTransactionsLastMinute()
        {
            HttpResponseMessage response = client.GetAsync($"{serverAppPath}/api/transaction/lastminute").Result;
            string jsonTransactionsLastMinute = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<IEnumerable<Transaction>>(jsonTransactionsLastMinute);
        }

        public static string GetTransactions()
        {
            HttpResponseMessage response = client.GetAsync($"{serverAppPath}/api/transaction").Result;
            string jsonTransactions = response.Content.ReadAsStringAsync().Result;
            return jsonTransactions;
        }

        public static IEnumerable<Vehicle> GetVehicles()
        {
            HttpResponseMessage response = client.GetAsync($"{serverAppPath}/api/vehicles").Result;
            string jsonVehicles = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<IEnumerable<Vehicle>>(jsonVehicles);
        }

        public static bool AddVehicle(Vehicle newVehicle)
        {
            string jsonNewVehicle = JsonConvert.SerializeObject(newVehicle);

            var content = new StringContent(jsonNewVehicle, Encoding.UTF8, "application/json");

            var response = client.PostAsync($"{serverAppPath}/api/vehicles", content).Result;

            return response.IsSuccessStatusCode;
        }

        public static void RemoveVehicle(int id)
        {
            client.DeleteAsync($"{serverAppPath}/api/vehicles/{id}");
        }

        public static void TopUpBalanceVehicle(Vehicle vehicle)
        {
            string jsonVehicle = JsonConvert.SerializeObject(vehicle);

            var content = new StringContent(jsonVehicle, Encoding.UTF8, "application/json");

            client.PutAsync($"{serverAppPath}/api/vehicles/{vehicle.ID}", content).Wait();
        }

        public static int CountVehicle()
        {
            HttpResponseMessage response = client.GetAsync($"{serverAppPath}/api/vehicles/count").Result;
            string jsonTransactions = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<int>(jsonTransactions);
        }
    }
}
