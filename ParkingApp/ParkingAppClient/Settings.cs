﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace ParkingApp
{
    [DataContract]
    class Settings
    {
        public static bool IsBlocked { get; set; } = false;
        [DataMember]
        [JsonProperty("StartBalance")]
        public static double StartBalance { get; set; }
        [DataMember]
        [JsonProperty("SpaceParking")]
        public static int SpaceParking { get; set; }
        [DataMember]
        [JsonProperty("PeriodicityPayment")]
        public static double PeriodicityPayment { get; set; }
        [DataMember]
        [JsonProperty("Rate")]
        public static Dictionary<string, double> Rate { get; set; } = new Dictionary<string, double>();
        [DataMember]
        [JsonProperty("CoefficientFine")]
        public static double CoefficientFine { get; set; }
    }
}
