﻿using ParkingAppServer;
using ParkingAppServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingAppServer.Services
{
    public class Vehicle
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double Balance { get; set; }
        public static int Amount { get; set; }

        public Vehicle ()
        {
            ID = Amount;
            Amount++;
        }
        public string GetNameVehicle()
        {
            return $"{Name} (ID: {ID})";
        }

        public Transaction WithdrawMoney()
        {
            double rate = Settings.Rate[Name];

            if (Balance <= 0)
                rate *= Settings.CoefficientFine;

            Balance -= rate;
            ParkingService.Balance += rate;

            return new Transaction()
            {
                TransactionID = Transaction.AmountTransaction,
                IDVehicle = ID,
                Time = DateTime.Now,
                Sum = rate
            };
        }
    }
}
