using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.IO;

namespace ParkingAppServer.Services {
    public class SettingsService {
        private HttpClient _settings;
        Settings settings;

        public SettingsService() {
            _settings = new HttpClient();

           settings = new Settings();
        }

        public Settings GetSettings()
        {
            return settings;
        }

        public void PostSettings(Settings newSettings)
        {
            settings = newSettings;
        }
    }
}