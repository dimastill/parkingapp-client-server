﻿using ParkingAppServer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingAppServer.Models
{
    public class Truck : Vehicle
    {
        public Truck()
        {
            Name = "Truck";
        }
    }
}
