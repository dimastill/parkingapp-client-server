﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkingAppServer.Models;
using ParkingAppServer.Services;

namespace ParkingAppServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        // GET: api/Vehicles
        [HttpGet]
        public IEnumerable<Vehicle> Get()
        {
            return ParkingService.Vehicles;
        }

        [HttpGet("{id}")]
        public Vehicle Get(int id)
        {
            return ParkingService.FindVehicle(id);
        }

        [Route("count")]
        [HttpGet]
        public int Count()
        {
            return ParkingService.Vehicles.Count;
        }

        // POST: api/Vehicle
        [HttpPost]
        public void Post([FromBody] Vehicle value)
        {
            ParkingService.AddVehicle(value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            ParkingService.RemoveVehicle(id);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Vehicle value)
        {
            ParkingService.Vehicles[id] = value;
        }
    }
}
