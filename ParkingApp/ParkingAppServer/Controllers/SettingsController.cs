using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ParkingAppServer.Services;

namespace ParkingAppServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SettingsController : ControllerBase
    {
        private SettingsService settingsService;

        public SettingsController()
        {
            settingsService = new SettingsService();
        }
        // GET api/values
        [HttpGet]
        public string Get()
        {
            return JsonConvert.SerializeObject(settingsService.GetSettings(), Formatting.Indented);
        }

        [HttpPost]
        public void Post([FromBody] Settings value)
        {
            settingsService.PostSettings(value);
        }
    }
}
