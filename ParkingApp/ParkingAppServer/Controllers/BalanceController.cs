using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ParkingAppServer.Services;

namespace ParkingAppServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BalanceController : ControllerBase
    {
        private ParkingService parkingService;

        public BalanceController()
        {
            parkingService = ParkingService.GetInstance();
        }

        [HttpGet]
        public string Get()
        {
            return JsonConvert.SerializeObject(ParkingService.Balance, Formatting.Indented);
        }

        [Route("lastminute")]
        [HttpGet]
        public string GetMoneyEarnedLastMinute()
        {
            return JsonConvert.SerializeObject(ParkingService.GetMoneyEarnedLastMinute(), Formatting.Indented);
        }
    }
}
