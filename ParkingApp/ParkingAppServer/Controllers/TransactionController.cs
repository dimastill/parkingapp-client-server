﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkingAppServer.Models;
using ParkingAppServer.Services;

namespace ParkingAppServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        // GET: api/Transaction
        [HttpGet]
        public string Get()
        {
            return ParkingService.ReadLog();
        }

        [Route("lastminute")]
        [HttpGet]
        public IEnumerable<Transaction> TransactionLastMinutes()
        {
            return ParkingService.FindTransactionsLastMinute();
        }
    }
}
